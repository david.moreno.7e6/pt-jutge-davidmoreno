
import org.postgresql.util.PSQLException
import java.nio.file.Path
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.InputMismatchException
import java.util.NoSuchElementException
import java.util.Scanner
import kotlin.io.path.*

val sc = Scanner(System.`in`)
val yellow = "\u001B[33m"
val blue = "\u001B[34m"
val purple = "\u001B[35m"
val green = "\u001B[32m"
val red = "\u001B[31m"

fun main() {
    try {
        //Connexió a la base de dades
        val jdbcUrl = "jdbc:postgresql://localhost:5432/jutge"
        val connection = DriverManager
        //A la funció getConnection he posat el user i password corresponents a la meva BD
            .getConnection(jdbcUrl, "postgres", "Simba-123")
        //Funció del text d'introducció al programa
        introduccio()
        //Inicialitzem la variable problemes amb el retorn de la funció getBDdata
        val problemes = getBDdata(connection)
        val listResolucions = resolucions(problemes)
        //Depenent del rang s'executa una part del codi o altre
        if (rango() == "NIETA") {
            val rango = Nieta()
            rango.introduccion()
            rango.menuOpciones()
            menuNieta(problemes, sc, listResolucions, connection)
        } else {
            val rango = Abuelo()
            rango.introduccio()
            rango.menuPrincipal()
            menuAbuelo(problemes, sc, listResolucions, connection)
        }
        println()
        println(green + "TENGO MÁS HISTORIAS EN MI RECUERDO HIJA. VUELVE CUANDO QUIERAS !")
    }
    //Capturem un error relacionat amb la BD i altre per si hi ha un error desconegut.
    catch (e: SQLException){
        println(red + "Error al conectarse a la base de datos. Disculpe las molestias. Error: ${e.message}")
    }
    catch (e: Exception){
        println(red + "Error inesperado. Error: ${e.message}")
    }
}
/*
Funció que ens serveix per agafar les dades de la BD i inicialitzar la variable problemes.De tal forma, amb aquesta variable
podem treballar amb les dades en cas de problemes de desconexió a la BD durant l'execució del programa.
Aquesta funció la cridarem durant diferents parts del codi per anar actualitzant les dades.
 */
fun getBDdata(connection: Connection): MutableList<Problema> {
    val problemes= mutableListOf<Problema>()
    try {
        //Rebem les dades de la taula Problema ordenades per ID
        val queryProblemes = connection.prepareStatement("SELECT * FROM Problema ORDER BY ID_PROBLEMA")
        val resultProblemes = queryProblemes.executeQuery()
        //Conjunt de variables que contenen MutableList amb les dades de cadascun dels problemes una vegada s'afegeixen al while
        val idsProblemes= mutableListOf<Int>()
        val titulosProblemas= mutableListOf<String>()
        val enuncitasProblemes= mutableListOf<String>()
        val resolucionsProblemes= mutableListOf<String>()
        val resoltProblemes= mutableListOf<Boolean>()
        val intentsProblemes= mutableListOf<Int>()
        while (resultProblemes.next()) {
            idsProblemes.add(resultProblemes.getInt("id_problema"))
            titulosProblemas.add(resultProblemes.getString("titulo"))
            enuncitasProblemes.add(resultProblemes.getString("enunciat"))
            resolucionsProblemes.add(resultProblemes.getString("resolucio"))
            resoltProblemes.add(resultProblemes.getBoolean("resolt"))
            intentsProblemes.add(resultProblemes.getInt("intents"))
        }
        /*
        Una vegada tenim les dades iterem els ids i per cadascun dels ids obtenim les dades de la taula prova_privat,
        prova_public i intent
         */
        for (i in 0..idsProblemes.lastIndex){
            val queryJocProvesPrivat= connection.prepareStatement("SELECT * FROM prova_privat where id_problema= ${idsProblemes[i]}")
            val resultProvesPrivat = queryJocProvesPrivat.executeQuery()
            var inputPrivat=""
            var outputPrivat= ""
            while (resultProvesPrivat.next()){
                inputPrivat= resultProvesPrivat.getString("input")
                outputPrivat= resultProvesPrivat.getString("output")
            }
            val queryJocProvesPublic= connection.prepareStatement("SELECT * FROM prova_public where id_problema= ${idsProblemes[i]}")
            val resultProvesPublic = queryJocProvesPublic.executeQuery()
            var inputPublic=""
            var outputPublic= ""
            while (resultProvesPublic.next()){
                inputPublic= resultProvesPublic.getString("input")
                outputPublic= resultProvesPublic.getString("output")
            }
            val queryIntents= connection.prepareStatement("SELECT * FROM intent where id_problema= ${idsProblemes[i]}")
            val resultIntents = queryIntents.executeQuery()
            var intents= mutableListOf<String>()
            while (resultIntents.next()){
                intents.add(resultIntents.getString("intent"))
            }
            //Una vegada obtenim totes les dades d'un problema, l'afegim a la variable problemes
            problemes.add(Problema(titulosProblemas[i], enuncitasProblemes[i], resolucionsProblemes[i], JocProvesPublic(inputPublic,outputPublic),JocProvesPrivate(inputPrivat, outputPrivat),resoltProblemes[i],intentsProblemes[i],intents))
            return problemes
        }
    }
    //Capturem un error relacionat amb la BD i altre per si hi ha un error desconegut.
    catch (e: PSQLException){
        println(red + "Error con la base de datos. Error: ${e.message}")
    }
    catch (e: Exception){
        println(red + "Error inesperado. Error: ${e.message}")
    }
    return problemes
}

//Funció per obtenir les resolucions de cada problema
fun resolucions( problemes: MutableList<Problema>): MutableList<String> {
    val resolucions= mutableListOf<String>()
    for (i in problemes){
        resolucions.add(i.resolucio)
    }
    return resolucions
}

    fun introduccio() {
        println(yellow + "- Abuelo! Abuelo! Cuéntame una história de nuevo porfi...")
        println("Decía Sara entusiasmada, pues le encantaba escuchar durante horas los relatos de su abuelo Juan.")
        println("- Está bien hija, hoy te voy a contar la história de un héroe un tanto singular")
        println("Decía el abuelo Juan con una sonrisa y una mirada melancólica.")
        println("- ¿Singular porqué abuelo?")
        println("Preguntaba Sara sin estar segura del significado de la palabra singular.")
        println(
            "- No es el héroe común hija. De hecho, odiaba ser un héroe y eso le hacía aún mas especial." +
                    " Pero, cómo una vez le dije:"
        )
        println("  - No puedes cambiar lo que eres por muy granuja que seas Antonio.")
        println("- ¿Antonio?¿Qué nombre de heróe es ese abuelo?")
        println("Dijo Sara entre sorprendida y decepcionada.")
        println(
            "- El nombre de un heróe real, de carne y hueso. Todo el mundo lo conocía como Toni, si te parece" +
                    "  mas 'guay'."
        );println(
            "  Bueno, basta de presentaciones. Te contaré sus mejores hazañas, por llamarlas de" +
                    "  alguna forma..."
        )
        println("Reía el abuelo al acabar su frase y se disponía a relatar la historia.")
        println()
    }
//Funció on l'usuari escull el rang desitjat
    fun rango(): String {
        println(green + """
            ████████╗░█████╗░███╗░░██╗██╗░░░██╗██╗  ████████╗██╗░░██╗███████╗
            ╚══██╔══╝██╔══██╗████╗░██║╚██╗░██╔╝╚═╝  ╚══██╔══╝██║░░██║██╔════╝
            ░░░██║░░░██║░░██║██╔██╗██║░╚████╔╝░░░░  ░░░██║░░░███████║█████╗░░
            ░░░██║░░░██║░░██║██║╚████║░░╚██╔╝░░░░░  ░░░██║░░░██╔══██║██╔══╝░░
            ░░░██║░░░╚█████╔╝██║░╚███║░░░██║░░░██╗  ░░░██║░░░██║░░██║███████╗
            ░░░╚═╝░░░░╚════╝░╚═╝░░╚══╝░░░╚═╝░░░╚═╝  ░░░╚═╝░░░╚═╝░░╚═╝╚══════╝

            ██╗░░██╗██╗░░░██╗███╗░░░███╗██████╗░██╗░░░░░███████╗  ██╗░░██╗███████╗██████╗░░█████╗░
            ██║░░██║██║░░░██║████╗░████║██╔══██╗██║░░░░░██╔════╝  ██║░░██║██╔════╝██╔══██╗██╔══██╗
            ███████║██║░░░██║██╔████╔██║██████╦╝██║░░░░░█████╗░░  ███████║█████╗░░██████╔╝██║░░██║
            ██╔══██║██║░░░██║██║╚██╔╝██║██╔══██╗██║░░░░░██╔══╝░░  ██╔══██║██╔══╝░░██╔══██╗██║░░██║
            ██║░░██║╚██████╔╝██║░╚═╝░██║██████╦╝███████╗███████╗  ██║░░██║███████╗██║░░██║╚█████╔╝
            ╚═╝░░╚═╝░╚═════╝░╚═╝░░░░░╚═╝╚═════╝░╚══════╝╚══════╝  ╚═╝░░╚═╝╚══════╝╚═╝░░╚═╝░╚════╝░
        """.trimIndent())
        println()
        println(blue + "RANGO:")
        print(purple + "NIETA");println(yellow + "    ABUELO")
        val sc = Scanner(System.`in`)
        val correct = false
        while (correct == false) {
            try {
                val rangoInput = sc.next()
                if (rangoInput.uppercase() == "NIETA") {
                    return rangoInput.uppercase()
                } else if (rangoInput.uppercase() == "ABUELO") {
                    return rangoInput.uppercase()
                } else {
                    println(red + "Opción no válida...")
                }
            }
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos. ")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
        }
        return ""
    }
//Una vegada escull el rang, l'usuari té diferents funcions segons el menú corresponent al rang

    fun menuNieta(problemes: MutableList<Problema>, sc: Scanner, listResolucions: MutableList<String>, connection: Connection) {
        var correct = false
        while (!correct) {
            try {
                val input = sc.nextInt()
                when (input) {
                    1 -> {
                        Nieta().continuar( listResolucions,connection)
                        menuNieta(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    2 -> {
                        Nieta().hazanyas(listResolucions,connection)
                        menuNieta(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    3 -> {
                        Nieta().historic(connection)
                        menuNieta(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    4 -> {
                        Nieta().ayuda()
                        menuNieta(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    5 -> {
                        correct = true
                    }

                    else -> {
                        println(red + "El abuelo está esperando...")
                    }
                }
            }
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos.")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
        }
    }

    fun menuAbuelo(problemes: MutableList<Problema>, sc: Scanner, listResolucions: MutableList<String>, connection: Connection){
        var correct = false
        while (!correct) {
            try {
                val input = sc.nextInt()
                when (input) {
                    1 -> {
                        Abuelo().afegir(problemes,connection)
                        menuAbuelo(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    2 -> {
                        Abuelo().nota(problemes)
                        menuAbuelo(problemes, sc, listResolucions,connection)
                        correct = true
                    }

                    3 -> {
                        correct = true
                    }
                    else -> {
                        println(red + "Estoy tan mayor que no veo ni las teclas...")
                    }
                }
            }
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos.")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
            }
    }

import org.postgresql.util.PSQLException
import java.sql.Connection
import java.util.InputMismatchException

class Abuelo() {
    fun introduccio(){
        println(blue + "Tantos recuerdos... La edad no perdona. Vamos a repasar historias\no a recordar nuevas jeje")
    }
    fun menuPrincipal(){
        println(
            purple + """
                
            ▀█▀ █▀█ █▄░█ █▄█ ▀   ▀█▀ █░█ █▀▀   █░█ █░█ █▀▄▀█ █▄▄ █░░ █▀▀   █░█ █▀▀ █▀█ █▀█
            ░█░ █▄█ █░▀█ ░█░ ▄   ░█░ █▀█ ██▄   █▀█ █▄█ █░▀░█ █▄█ █▄▄ ██▄   █▀█ ██▄ █▀▄ █▄█
                
            1. AÑADIR UNA HAZAÑA
            2. REVISAR SI MI NIETA ESTÁ ATENTA
            3. IRSE AL BINGO
        """.trimIndent()
        )
    }
//Funció on l'usuari introdueix totes les dades del problema a afegir.
    fun afegir( problemes: MutableList<Problema>, connection: Connection){
        try {
            println("Titol del problema:")
            sc.nextLine()
            val titol=sc.nextLine()
            println("Enunciat:")
            val enunciat= sc.nextLine()
            println("Text al resoldre el problema:")
            val resolucio= sc.nextLine()
            println("Input public:")
            val input= sc.nextLine()
            println("Output public:")
            val output= sc.nextLine()
            println("Input privat:")
            val inputPrivat= sc.nextLine()
            println("Output ptivat:")
            val outputPrivat= sc.next()
            //En aquest bloc de codi s'insereixen les dades a la seva taula corresponent de la base de dades
            val insertProblema = connection.prepareStatement("INSERT INTO PROBLEMA ( titulo, enunciat, resolucio, resolt, intents) values ('$titol','$enunciat','$resolucio',false,0)")
            insertProblema.executeUpdate()
            val insertProvaPublic= connection.prepareStatement("INSERT INTO PROVA_PUBLIC(input,output,id_problema) VALUES ('$input','$output',${problemes.size+1})")
            insertProvaPublic.executeUpdate()
            val insertProvaPrivat= connection.prepareStatement("INSERT INTO PROVA_PRIVAT(input,output,id_problema) VALUES ('$inputPrivat','$outputPrivat',${problemes.size+1})")
            insertProvaPrivat.executeUpdate()
            //Inicialitzem el problema a una variable i l'afegim a la llista de problemes
            val problema= Problema(titol, enunciat, resolucio, JocProvesPublic(input,output), JocProvesPrivate(inputPrivat,outputPrivat),false,0, mutableListOf())
            problemes.add(problema)
            menuPrincipal()
        }
        //Capturem esl errors relacions amb la base de dades, introducció de dades per part de l'usuari i errors desconeguts.
        catch (e: InputMismatchException){
            println(red + "Error: Has introducido carácteres incorrectos.")
        }
        catch (e: PSQLException){
            println(red + "Error en la conexión con la base de datos. Error: ${e.message}")
        }
        catch (e: Exception){
            println(red + "Error inesperado. Error: ${e.message}")
        }
    }
    //Amb la llista de problemes calculem la nota. La nota per problema va en funció del número de problemes existents.
    fun nota(problemes: MutableList<Problema>){
        if(problemes.isEmpty()){
            menuPrincipal()
            throw Exception("No hay problemas creados.")
        }
        var nota=0.0
        val notaProblema= 10.0/problemes.size
        for (i in problemes){
            if (i.resolt){
                when(i.intents){
                    1-> {
                        nota += notaProblema
                        println(blue + i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema)
                    }
                    in 2..3->{
                        nota += notaProblema*0.8
                        println(blue + i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema*0.8)
                    }
                    in 4..5->{
                        nota += notaProblema*0.6
                        println(blue+ i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema*0.6)
                    }
                    in 6..10->{
                        nota += notaProblema*0.5
                        println(blue + i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema*0.5)
                    }
                    in 11..50->{
                        nota += notaProblema*0.3
                        println(blue + i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema*0.25)
                    }
                    else->{
                        nota+= notaProblema*0.1
                        println(blue + i.titulo)
                        print(green + "NOTA: ");println(red + notaProblema*0.1)
                    }
                }
            }
            else{
                nota += 0
                println(blue + i.titulo)
                print(green + "NOTA: ");println(red + "0")
            }
        }
        println(green + """
                
                █▄░█ █▀█ ▀█▀ ▄▀█   █▀▀ █ █▄░█ ▄▀█ █░░
                █░▀█ █▄█ ░█░ █▀█   █▀░ █ █░▀█ █▀█ █▄▄
                
                
                             $nota
            """.trimIndent())
        menuPrincipal()
    }


}

import java.sql.Connection
import java.sql.SQLException
import java.util.*

class Nieta(){
    fun introduccion() {
        println(blue + "Cuanto tiempo pequeña. ¿Has venido para ver si el abuelo te cuenta\n otra de sus historias ?")
    }

    fun menuOpciones() {
        println(
            purple + """
                
            ▀█▀ █▀█ █▄░█ █▄█ ▀   ▀█▀ █░█ █▀▀   █░█ █░█ █▀▄▀█ █▄▄ █░░ █▀▀   █░█ █▀▀ █▀█ █▀█
            ░█░ █▄█ █░▀█ ░█░ ▄   ░█░ █▀█ ██▄   █▀█ █▄█ █░▀░█ █▄█ █▄▄ ██▄   █▀█ ██▄ █▀▄ █▄█
                
            1. CONTINUAR CON LA HISTORIA DEL ABUELO
            2. HAZAÑAS DE TONY
            3. HAZAÑAS RESUELTAS
            4. PREGUNTAR AL ABUELO
            5. DESPEDIRSE DEL ABUELO
        """.trimIndent()
        )
    }
    //Funció que s'encarrega de gestionar els problemes i crida a la funció jutge per tal d'introduir respostes
    fun problemesMain(problemes: MutableList<Problema>, listResolucions: MutableList<String>, input: Int): Boolean {
        try {
            problemes[input - 1].enunciat
            println("""
           Dejar de escuchar al abuelo ["SALIR"] 
        """.trimIndent())
            if (problemes[input - 1].jutge(problemes[input - 1].jocProvesPrivat.privateOutput)){
                println(purple + "INTENTOS: ${problemes[input - 1].intents}")
                println(red + "LISTA DE INTENTOS: ${problemes[input - 1].llistaInputs}")
                println(green + "RESUELTO: ${problemes[input - 1].resolt}")
                println()
                println(blue + listResolucions[input-1])
                return true
            }
            else {
                return false
            }
        }
        //Error que captura un error amb l'index de la llista problemes
        catch (e: IndexOutOfBoundsException) {
            println(red + "Error en el índice de la colección.")
            return false
        }
    }
    //Funció que ens permet anar actualitzant les dades a la base de dades mentre el programa segueix en funcionament
    fun update(problemes: MutableList<Problema>, posicio: Int, connection: Connection){
        try {
            val id=posicio+1
            for (i in problemes){
                val updateProblemes= connection.prepareStatement("UPDATE Problema SET resolt= ${problemes[posicio].resolt}, intents=${problemes[posicio].intents} WHERE ID_PROBLEMA = $id")
                updateProblemes.executeUpdate()
            }
            for (i in problemes[posicio].llistaInputs){
                val insertInputs= connection.prepareStatement("INSERT INTO INTENT(intent,id_prova_privat,id_problema) values ('$i',$id, $id)")
                insertInputs.executeUpdate()
            }
        }
        //Capturem un error relacionat amb la BD i altre per si hi ha un error desconegut.
        catch (e: SQLException) {
            println("Error al actualizar o insertar datos en la base de datos. Error: ${e.message}")
        } catch (e: Exception) {
            println("Error inesperado: ${e.message}")
        }

    }
    //Funció que ens permet restaurar les dades d'un problema resolt en cas que es vulgui tornar a fer.
    fun resolt(problemes: MutableList<Problema>, posicio: Int): Boolean {
        if (problemes[posicio].resolt){
            println(red + """
                Nieta, no estás atenta a mis historias?! Ya te conté esta hazaña...
                De todas formas, quieres volver a escucharla? Dime "SI" y borraremos
                de nuestra memoria lo que sucedió y lo resolveremos de nuevo, di "NO"
                y dejaremos el recuerdo tal y como está.
            """.trimIndent())
            var correct=false
            while (!correct){
                try {
                    val input= sc.next()
                    if (input.uppercase()=="SI"){
                        return true
                    }
                    else if(input.uppercase()=="NO"){
                        return false
                    }
                    else {
                        println(blue + "Repite nieta, no te he escuchado bien...")
                    }
                }
                //Captura l'error d'introducció de dades per tal de l'usuari i un error desconegut.
                catch (e: InputMismatchException){
                    println(red + "Error: Has introducido carácteres incorrectos.")
                    break
                }
                catch (e: Exception){
                    println(red + "Error inesperado. Error: ${e.message}")
                    break
                }
            }
        }
        return false
    }
    //Funció que ens permet seleccionar un problema en concret.

    fun hazanyas( listResolucions: MutableList<String>, connection: Connection) {
        val problemes = getBDdata(connection)
        var numeracio = 0
        println(green+ """
            
            █░█ ▄▀█ ▀█ ▄▀█ █▄░█ ▄▀█ █▀
            █▀█ █▀█ █▄ █▀█ █░▀█ █▀█ ▄█
            
        """.trimIndent())
        //Llistat de problemes existents
        for (i in problemes) {
            numeracio++
            println(green + "$numeracio. ${i.titulo}")
        }
        //Variable que ens permet anar enrere
        val atras= problemes.size + 1
        println("$atras. ATRÁS")
        println()
        var correct = false
        while (!correct) {
            try {
                val input = sc.nextInt()
                when (input) {
                    1 -> {
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }

                    2 -> {
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }

                    3 -> {
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }

                    4 -> {
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }

                    5 -> {
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }
                    in 6..problemes.size->{
                        correct=hazanyasResult(problemes, input, connection, listResolucions)
                    }

                    atras-> {
                        menuOpciones()
                        correct = true
                    }

                    else -> {
                        println(red + "El abuelo no recuerda esa hazaña...")
                    }
            }
            }
            //Captura l'error d'introducció de dades per tal de l'usuari i un error desconegut.
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos.")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
        }
    }
    //Funció que ens monstra l'historic de problemes i les dades relacionades.
    fun historic(connection: Connection) {
        val problemes= getBDdata(connection)
        println(blue + """
            
            █░█ █ █▀ ▀█▀ █▀█ █▀█ █ █▀▀ █▀█
            █▀█ █ ▄█ ░█░ █▄█ █▀▄ █ █▄▄ █▄█
            
        """.trimIndent())
        for (i in problemes) {
            println(blue + i.titulo)
            println(purple + "INTENTOS: ${i.intents}")
            println(red + "LISTA DE INTENTOS: ${i.llistaInputs}")
            println(green + "RESUELTO: ${i.resolt}")
            println()
        }
        menuOpciones()
    }
    fun menuAyuda(){
        println(
            blue + """
                
            ▄▀█ █▄█ █░█ █▀▄ ▄▀█
            █▀█ ░█░ █▄█ █▄▀ █▀█
                
            1. CONTINUAR CON LA HISTORIA DEL ABUELO
            2. HAZAÑAS DE TONY
            3. HAZAÑAS RESUELTAS
            4. PREGUNTAR AL ABUELO
            5. DESPEDIRSE DEL ABUELO
        """.trimIndent()
        )
    }
    //Instruccions per l'usuari
    fun ayuda() {
        println(blue + "En que necesitas ayuda nieta ?")
        menuAyuda()
        println("6. ATRÁS")
        var correct = false
        while (!correct) {
            try {
                val input = sc.nextInt()
                when (input) {
                    1 -> {
                        println(
                            yellow + """
                        Continuaré contando la historia de Tony por dónde la dejamos. Soy un poco mayor,
                        quizás te he contado la historia desordenada. Así que la retomaré por orden
                        cronológico.
                    """.trimIndent()
                        )
                        ayuda()
                        correct = true
                    }

                    2 -> {
                        println(
                            yellow + """
                        Te contaré la hazaña de Tony que me digas. Si todavía la recuerdo...jeje
                    """.trimIndent()
                        )
                        ayuda()
                        correct = true
                    }

                    3 -> {
                        println(
                            yellow + """
                        Haré un repaso para saber qué hazañas te he contado y cuales hemos dejado
                        a medias.
                    """.trimIndent()
                        )
                        ayuda()
                        correct = true
                    }

                    4 -> {
                        println(
                            yellow + """
                        “No existen preguntas sin respuesta, solo preguntas mal formuladas.”
                    """.trimIndent()
                        )
                        ayuda()
                        correct = true
                    }

                    5 -> {
                        println(
                            yellow + """
                        Llegará la hora de irte a clases de Piano. Como pasa el tiempo...
                    """.trimIndent()
                        )
                        ayuda()
                        correct = true
                    }

                    6 -> {
                        menuOpciones()
                        correct = true
                    }

                    else -> {
                        println(red + "Mmm no sé cómo ayudarte con eso nieta...")
                    }
                }
            }
            //Captura l'error d'introducció de dades per tal de l'usuari i un error desconegut.
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos.")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
        }
    }
    //Funció que comprova si tots els problemes han estat resolts i permet reiniciar l'historia principal de problemes.
    fun comprovarHistoria(problemes: MutableList<Problema>, connection: Connection){
        var resolt= 0
        for (i in problemes){
            if(i.resolt){
                resolt++
            }
        }
        if(resolt==problemes.size){
            println(blue + """
                Ya te he contado la historia completa de Tony...
                1. ESCUCHAR HISTORIA COMPLETA DE NUEVO
                2. ESPERAR A QUE EL ABUELO RECUERDE MÁS HISTORIAS
            """.trimIndent())
            var correct= false
            while(!correct){
                try {
                    val input= sc.nextInt()
                    if(input == 1){
                        for (i in problemes){
                            i.resolt = false
                            i.intents = 0
                            i.llistaInputs = mutableListOf()
                        }
                        val dropIntents= connection.prepareStatement("DELETE FROM INTENT")
                        dropIntents.executeUpdate()
                        val updateProblemes= connection.prepareStatement("UPDATE Problema SET resolt= false, intents= 0")
                        updateProblemes.executeUpdate()
                        correct=true
                    }
                    else if(input==2){
                        correct=true
                    }
                    else{
                        println(red + "El abuelo le da una calada a su puro mientras espera una respuesta.")
                    }
                }
                //Captura l'error d'introducció de dades per tal de l'usuari i un error desconegut.
                catch (e: InputMismatchException){
                    println(red + "Error: Has introducido carácteres incorrectos.")
                    break
                }
                catch (e: Exception){
                    println(red + "Error inesperado. Error: ${e.message}")
                    break
                }
            }
        }
    }
    //Funció que comprova quins problemes han estat resolts i els que no, els mostra cronológicament.
    fun continuar(listResolucions: MutableList<String>, connection: Connection) {
        println(green + "Mmmm por dónde íbamos... Ah si, ya lo recuerdo.")
        var problema = 0
        var problemes= getBDdata(connection)
        comprovarHistoria(problemes, connection)
        for (i in problemes) {
            problemes= getBDdata(connection)
            problema++
            if (!i.resolt) {
                when (problema) {
                    1 -> {
                        continuarResult(problemes, problema, listResolucions, connection)
                    }

                    2 -> {
                        continuarResult(problemes, problema, listResolucions, connection)
                    }

                    3 -> {
                        continuarResult(problemes, problema, listResolucions, connection)
                    }

                    4 -> {
                        continuarResult(problemes, problema, listResolucions, connection)
                    }

                    5 -> {
                        continuarResult(problemes, problema, listResolucions, connection)
                    }
                    in 6..problemes.size->{
                        continuarResult(problemes, problema, listResolucions, connection)
                    }
                }
            }

        }
        menuOpciones()
    }
    //Funció que cridem a la funció continuar() per tal de resoldre problemes.
    fun continuarResult(problemes: MutableList<Problema>, problema: Int, listResolucions: MutableList<String>, connection: Connection){
        println(blue + problemes[problema - 1].enunciat)
        problemes[problema - 1].jocProvesPublic.problemes()
        problemes[problema - 1].jocProvesPrivat.problemes()
        if (problemesMain(problemes, listResolucions, problema)){
            update(problemes,problema-1,connection)
        }
    }
    /*
    Funció que cridem a la funcio hazanyas() on comprovem si el problema ha estat resolt. En cas positiu l'usuari té l'opció de tornar-ho a fer.
    Si vol, reinicia els intents i els elimina de la base de dades i reinicia les altres dades. Després l'usuari pot fer el problema de nou.
    En cas de no estar resolt, l'usuari pot resoldre el problema directament.
     */
    fun hazanyasResult(problemes: MutableList<Problema>,input: Int,connection: Connection,listResolucions: MutableList<String>): Boolean{
        if (problemes[input-1].resolt){
            if (resolt(problemes, input-1)){
                val dropIntents= connection.prepareStatement("DELETE FROM INTENT WHERE ID_PROBLEMA = $input")
                dropIntents.executeUpdate()
                problemes[input-1].resolt = false
                problemes[input-1].llistaInputs = mutableListOf()
                problemes[input-1].intents= 0
                println(blue + problemes[input - 1].enunciat)
                problemes[input - 1].jocProvesPublic.problemes()
                problemes[input-1].jocProvesPrivat.problemes()
                problemesMain(problemes, listResolucions, input)
                update(problemes,input-1,connection)
                hazanyas( listResolucions,connection)
                return true
            }
            else{
                hazanyas(listResolucions,connection)
                return true
            }
        }
        else{
            println(blue + problemes[input - 1].enunciat)
            problemes[input - 1].jocProvesPublic.problemes()
            problemes[input-1].jocProvesPrivat.problemes()
            problemesMain(problemes, listResolucions, input)
            update(problemes,input-1,connection)
            hazanyas(listResolucions,connection)
            return true
        }
    }
}
import java.util.InputMismatchException

class Problema(
    val titulo: String,
    val enunciat: String,
    val resolucio: String,
    val jocProvesPublic: JocProvesPublic,
    val jocProvesPrivat: JocProvesPrivate,
    var resolt: Boolean,
    var intents: Int,
    var llistaInputs: MutableList<String>){
    /*
    En aquesta funció l'usuari introdueix la resposta al problema i té l'opció de sortir en cas de no voler continuar.
    Les respostes incorrectes s'afegeixen a una llista.
     */
    fun jutge(privateOutput: String): Boolean {
        var resolucio= ""
        while(resolucio!= privateOutput){
            try {
                resolucio= sc.next()
                if(resolucio.uppercase()== "SALIR"){
                    return false
                }
                intents++
                llistaInputs.add(resolucio)
                if (resolucio == privateOutput){
                    resolt= true
                    return true
                }

                else{
                    println(red + "Prueba de nuevo...")
                }
            }
            //Capturem un error relacionat amb l'introducció de dades per part de l'usuari i altre per si hi ha un error desconegut.
            catch (e: InputMismatchException){
                println(red + "Error: Has introducido carácteres incorrectos.")
                break
            }
            catch (e: Exception){
                println(red + "Error inesperado. Error: ${e.message}")
                break
            }
        }
        return false
    }
}